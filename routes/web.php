<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('login', [AuthController::class, 'index'])->name('login');
Route::post('login', [AuthController::class, 'userLogin']);

Route::group(['middleware' => 'user_auth'], function () {
    Route::get('home', function() {
        return redirect()->route('index');
    });
    Route::get('/', [HomeController::class, 'home'])->name('index');
    Route::get('create_author', [HomeController::class, 'create_author_form'])->name('create_author');
    Route::get('create_book', [HomeController::class, 'create_book_form'])->name('create_book');
    Route::get('delete_author/{id}', [HomeController::class, 'delete_author'])->name('delete_author');
    Route::get('delete_book/{id}', [HomeController::class, 'delete_book'])->name('delete_book');
    Route::get('view_author/{id}', [HomeController::class, 'view_author'])->name('view_author');
    Route::get('profile', [HomeController::class, 'profile'])->name('profile');

    Route::post('home', [HomeController::class, 'home'])->name('search_author');
    Route::post('create_author', [HomeController::class, 'create_author'])->name('create_author');
    Route::post('create_book', [HomeController::class, 'create_book'])->name('create_book');

    Route::get('logout', [AuthController::class, 'logout']);
});



