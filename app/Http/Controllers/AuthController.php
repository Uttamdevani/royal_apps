<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use \Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Http;
use validator;
use Auth;

class AuthController extends Controller
{
    function index() {
        return view('auth/login');
    }

    function userLogin(Request $request) {

        $this->validate($request,[
            'email'=>'required',
            'password'=>'required'
        ]);

        $client = new Client();

        $apiUrl = env('API_URL') . '/token';
        $response = Http::post($apiUrl, [
            'email' => $request->email,
            'password' => $request->password,
        ]);
        $responseBody = $response->getBody()->getContents();
        $userData = json_decode($responseBody, true);
        if($response->failed()) {
            return redirect()->route('login')->with('message', 'Invalid login credentials');
        } else {
            session()->put('user_data', $userData);            
            return redirect()->route('index');
        }
    }

    function logout() {
        Session::forget('user_data');

        return redirect('login');
    }
}
