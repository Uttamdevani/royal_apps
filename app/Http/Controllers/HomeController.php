<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use \Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Http;
use Validator, Redirect, Auth;

class HomeController extends Controller
{
    public $token = '';

    public $user_id = '';

    public $user_data = [];

    function __construct($token = '')
    {   
        $this->middleware(function ($request, $next){
            $user_data = session('user_data');    
            if (!empty($user_data)) {
                $this->token = $user_data['token_key'];
                $this->user_data = $user_data['user'];
                $this->user_id = $user_data['user']['id'];
                return $next($request);
            } else {
                return redirect('login');
            }                    
        });
    }

    function home(Request $request)
    {
        $apiUrl = env('API_URL') . '/authors';
        $search_param = !empty($request->search) ? $request->search : '';
        $query = $request->search ? ['query' => $search_param] : [];
        $page = $request->page ? $query['page'] = $request->page : [];
        $query['direction'] = "DESC";
        $response = Http::withToken($this->token)->get($apiUrl, $query);
        $author_data = $response->json();

        return view('index', [
            'user' => $this->user_data,
            'author_data' => $author_data,
            'author_list' => $author_data['items'],
            'search' => $search_param
        ]);
    }

    function create_author_form() {
        return view('author', ['user' => $this->user_data]);
    }

    function create_book_form() {
        $apiUrl = env('API_URL') . '/authors';

        $query['limit'] = 100000000;
        $response = Http::withToken($this->token)->get($apiUrl, $query);
        $author_data = $response->json();

        return view('book', ['author_list' => $author_data['items'], 'user' => $this->user_data]);
    }

    function create_author(Request $request) {
        $request_data = [
            "first_name" => $request->first_name,
            "last_name" => $request->last_name,
            "birthday" => $request->birth_date,
            "biography" => $request->biography,
            "gender" => $request->gender,
            "place_of_birth" => $request->place_of_birth
        ];
        $apiUrl = env('API_URL') . '/authors';
        $create_author = Http::withToken($this->token)->post($apiUrl, $request_data);
        $create_author = $create_author->json();

        if(!isset($create_author['status'])) {
            Session::flash('success', 'Author created successfully');
            return redirect()->route('index');
        } else {
            Session::flash('error', $create_author['detail']);
            return redirect()->route('index');
        }
    }

    function create_book(Request $request) {
        $request_data = [
            "author" => ['id' => $request->author_id],
            "title" => $request->title,
            "release_date" => $request->release_date,
            "description" => $request->description,
            "isbn" => $request->isbn,
            "format" => $request->book_format,
            "number_of_pages" => (int)$request->number_of_pages
        ];
        $apiUrl = env('API_URL') . '/books';
        $create_book = Http::withToken($this->token)->post($apiUrl, $request_data);
        $create_book = $create_book->json();

        if(!isset($create_book['status'])) {
            Session::flash('success', 'Books added successfully');
            return redirect()->route('index');
        } else {
            Session::flash('error', $create_book['detail']);
            return redirect()->route('index');
        }
    }

    function delete_author($id) {
        $apiUrl = env('API_URL') . '/authors/'.$id;
        $get_author_data = Http::withToken($this->token)->get($apiUrl);
        $get_author_data = $get_author_data->json();
        if(empty($get_author_data['books'])) {
            $delete_book = Http::withToken($this->token)->accept('*/*')->delete($apiUrl);
            $delete_book = $delete_book->json();
            if(!isset($delete_book['status'])) {
                Session::flash('success', 'Author deleted successfully');
                return redirect()->route('index');
            } else {
                Session::flash('error', $delete_book['detail']);
                return redirect()->route('index');
            }
        } else {
            Session::flash('error', 'Please remove author books first.');
            return redirect()->route('index');
        }
    }

    function view_author($id) {
        $apiUrl = env('API_URL') . '/authors/'.$id;
        $get_author_data = Http::withToken($this->token)->get($apiUrl);
        $get_author_data = $get_author_data->json();
        return view('view_author', ['user' => $this->user_data, 'author_data'=> $get_author_data]);
    }

    function delete_book($id) {
        $apiUrl = env('API_URL') . '/books/'.$id;
        $delete_book = Http::withToken($this->token)->accept('*/*')->delete($apiUrl);
        $delete_book = $delete_book->json();
        if(!isset($delete_book['status'])) {
            Session::flash('success', 'Book removed successfully.');
        } else {
            Session::flash('error', 'Somthing went wrong.');
        }
        return Redirect::back();
    }

    function profile() {
        return view('profile', ['user' => $this->user_data]);
    }
}
