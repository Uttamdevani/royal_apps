# royal_apps
royal_apps is a laravel project to manage authors and books.

## Requirements
- Local server like xampp or wamp
- PHP 8.1 Or higher


## Installation
- Clone the repository on the local server : https://gitlab.com/Uttamdevani/royal_apps.git

## How to run project
- Run this command in the project directory.
- php artisan serve --port=8080

## Credentials of client to login on a project
- Email: ahsoka.tano@royal-apps.io
- Password: Kryze4President

## Project Url
- http://localhost:8080
