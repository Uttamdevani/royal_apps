@extends('layouts.master')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-6">
            <table class="table table-border table-striped">
                <tr>
                    <th>Name</th>
                    <td>{{$user['first_name']}} {{$user['last_name']}}</td>
                </tr>
                <tr>
                    <th>Email</th>
                    <td>{{$user['email']}}</td>
                </tr>
                <tr>
                    <th>Sex</th>
                    <td>{{$user['gender']}}</td>
                </tr>
                <tr>
                    <th>Status</th>
                    <td>@if($user['active'] == "1") {{ "Active" }} @else {{ "Deactive" }} @endif</td>
                </tr>
                <tr>
                    <th>Email status</th>
                    <td>@if($user['email_confirmed'] == "1") {{ "Active" }} @else {{ "Deactive" }} @endif</td>
                </tr>
            </table>
        </div>
    </div>
</div>
@stop