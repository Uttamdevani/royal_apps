@extends('layouts.master')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-6">
            <table class="table table-border table-striped">
                <tr>
                    <th>Name</th>
                    <td>{{$author_data['first_name']}} {{$author_data['last_name']}}</td>
                </tr>
                <tr>
                    <th>Birthdate</th>
                    <td>{{$author_data['birthday']}}</td>
                </tr>
                <tr>
                    <th>Biography</th>
                    <td>{{$author_data['biography']}}</td>
                </tr>
                <tr>
                    <th>Sex</th>
                    <td>{{$author_data['gender']}}</td>
                </tr>
                <tr>
                    <th>Place of birth</th>
                    <td>{{$author_data['place_of_birth']}}</td>
                </tr>
            </table>
        </div>
    </div>
</div>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-10 mt-4">
            <h3>Books</h3>
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">Id</th>
                        <th scope="col">Title</th>
                        <th scope="col">Release date</th>
                        <th scope="col">Description</th>
                        <th scope="col">ISBN</th>
                        <th scope="col">Format</th>
                        <th scope="col">Number of pages</th>
                        <th scope="col" colspan="2" class="text-center">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($author_data['books'] as $book)
                    <tr>
                        <td>{{$book['id']}}</td>
                        <td>{{$book['title']}}</td>
                        <td>{{$book['release_date']}}</td>
                        <td>{{$book['description']}}</td>
                        <td>{{$book['isbn']}}</td>
                        <td>{{$book['format']}}</td>
                        <td>{{$book['number_of_pages']}}</td>
                        <td><a href="{{url('delete_book')}}/{{$book['id']}}"><button class="btn btn-danger">Delete</button></a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@stop