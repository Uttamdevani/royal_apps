@extends('layouts.master')
@section('content')
<div class="col-lg-8">
    <form method="post" action="{{url('create_book')}}">
        {{csrf_field()}}
        <h3 class="text-center mb-4 mt-4">Add Book</h3>
        <div class="form-group">
            <div class="mb-3">
                <label for="exampleFormControlInput1" class="form-label">First Name</label>
                <select class="form-select" name="author_id" aria-label="Default select example" required>
                    <option value="" selected>Select Author</option>
                    @foreach($author_list as $author)
                    <option value="{{$author['id']}}">{{$author['first_name']}} {{$author['last_name']}}</option>
                    @endforeach
                </select>
            </div>
            <div class="mb-3">
                <label for="exampleFormControlInput1" class="form-label">Title</label>
                <input type="text" name="title" class="form-control" placeholder="Book Title" required>
            </div>
            <div class="mb-3">
                <label for="exampleFormControlInput1" class="form-label">Release Data</label>
                <input type="date" name="release_date" class="form-control" required>
            </div>
            <div class="mb-3">
                <label for="exampleFormControlInput1" class="form-label">Description</label>
                <textarea name="description" class="form-control" required></textarea>
            </div>
            <div class="mb-3">
                <label for="exampleFormControlInput1" class="form-label">isbn</label>
                <input type="text" name="isbn" class="form-control" placeholder="" required>
            </div>
            <div class="mb-3">
                <label for="exampleFormControlInput1" class="form-label">Format</label><br>
                <input type="text" name="book_format" class="form-control" required>
            </div>
            <div class="mb-3">
                <label for="exampleFormControlInput1" class="form-label">Number of pages</label><br>
                <input type="text" name="number_of_pages" class="form-control" placeholder="200" required>
            </div>
            <div class="action">
                <button type="submit" name="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </form>
</div>
@stop