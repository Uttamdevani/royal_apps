<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Royal Apps</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ URL::asset('css/auth/login.css') }}">
</head>

<body>
    <div class="container">
        <div class="row justify-content-center align-items-center royal_apps__login_block">
            <div class="col-6 border">
                <h1 class="text-center">Royal Apps</h1>
                @if (session('message'))
                    <div class="alert alert-danger p-2 mt-3">
                        {{ session('message') }}
                    </div>
                @endif
                <form method="post" action="{{url('login')}}">
                    {{csrf_field()}}
                    <div class="mb-3">
                        <label for="exampleFormControlInput1" class="form-label">Email</label>
                        <input type="email" name="email" class="form-control" id="exampleFormControlInput1" placeholder="name@example.com" value="{{ old('email') }}">
                        <span class="text-danger">{{$errors->first('email')}}</span>
                    </div>
                    <div class="mb-3">
                        <label for="exampleFormControlTextarea1" class="form-label">Password</label>
                        <input type="password" name="password" placeholder="Enter password" class="form-control">
                        <span class="text-danger">{{$errors->first('password')}}</span>
                    </div>
                    <div class="royal_apps__actions_btn">
                        <button type="submit" name="submit" class="btn btn-primary mb-3 float-end">
                            Login
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>
</body>

</html>