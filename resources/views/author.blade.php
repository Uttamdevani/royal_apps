@extends('layouts.master')
@section('content')
<div class="col-lg-8">
    <form method="post" action="{{url('create_author')}}">
        {{csrf_field()}}
        <h3 class="text-center mb-4 mt-4">Add Author</h3>
        <div class="form-group">
            <div class="mb-3">
                <label for="exampleFormControlInput1" class="form-label">First Name</label>
                <input type="text" name="first_name" class="form-control" placeholder="John" required>
            </div>
            <div class="mb-3">
                <label for="exampleFormControlInput1" class="form-label">Last Name</label>
                <input type="text" name="last_name" class="form-control" placeholder="Michal" required>
            </div>
            <div class="mb-3">
                <label for="exampleFormControlInput1" class="form-label">Birthdate</label>
                <input type="date" name="birth_date" class="form-control" required>
            </div>
            <div class="mb-3">
                <label for="exampleFormControlInput1" class="form-label">Biography</label>
                <textarea name="biography" class="form-control" required></textarea>
            </div>
            <div class="mb-3">
                <label for="exampleFormControlInput1" class="form-label">Place of birth</label>
                <input type="text" name="place_of_birth" class="form-control" placeholder="USA" required>
            </div>
            <div class="mb-3">
                <label for="exampleFormControlInput1" class="form-label">Gender</label><br>
                <div class="form-check">
                    <input class="form-check-input" checked type="radio" name="gender" value="male" required>
                    <label class="form-check-label" for="flexRadioDefault1">
                        Male
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="gender" value="female" required>
                    <label class="form-check-label" for="flexRadioDefault2">
                        Female
                    </label>
                </div>
            </div>
            <div class="action">
                <button type="submit" name="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </form>
</div>
@stop