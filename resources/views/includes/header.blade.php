<div class="col-12 p-0">
    <nav class="navbar navbar-expand-lg bg-light">
        <div class="container-fluid">
            <a class="navbar-brand" href="{{ url('/') }}">{{ $user['first_name'] }} {{$user['last_name']}}</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="create_user">
                <a href="{{ url('create_author') }}"><button class="btn btn-primary">Add Author</button></a>
                <a href="{{ url('create_book') }}"><button class="btn btn-primary">Add Book</button></a>
                <a href="{{ url('profile') }}"><button class="btn btn-primary"><i class="fa fa-user"></i></button></a>
                <a href="{{ url('logout') }}"><button class="btn btn-danger"><i class="fa fa-sign-out"></i></button></a>
            </div>
        </div>
    </nav>
</div>