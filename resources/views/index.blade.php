@extends('layouts.master')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-10">
            <div class="row justify-content-center">
                <div class="col-10">
                    <form method="post" action="{{ url('home') }}" class="d-flex align-items-center">
                        {{csrf_field()}}
                        <input type="search" name="search" class="form-control mt-3" value="{{$search}}">
                        <input type="submit" name="submit" placeholder="Search here" class="btn btn-primary mt-3 ms-2">
                    </form>
                </div>
                @if($search)
                <div class="col-2 d-flex align-items-center mt-3">
                    <a href="{{ url('/') }}">
                        <button class="btn btn-secondary ms-2">Cancle</button>
                    </a>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row justify-content-center mt-4">
        <div class="col-lg-10">
            @if (session('success'))
            <div class="alert alert-success p-2 mt-3">
                {{ session('success') }}
            </div>
            @elseif (session('error'))
            <div class="alert alert-danger p-2 mt-3">
                {{ session('error') }}
            </div>
            @endif
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">Id</th>
                        <th scope="col">First name</th>
                        <th scope="col">Last name</th>
                        <th scope="col">Birthday</th>
                        <th scope="col">Gender</th>
                        <th scope="col">Place of birth</th>
                        <th scope="col" colspan="2" class="text-center">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($author_list as $author)
                    <tr>
                        <td>{{$author['id']}}</td>
                        <td>{{$author['first_name']}}</td>
                        <td>{{$author['last_name']}}</td>
                        <td>{{$author['birthday']}}</td>
                        <td>{{$author['gender']}}</td>
                        <td>{{$author['place_of_birth']}}</td>
                        <td class="text-white"><a href="{{url('view_author')}}/{{$author['id']}}"><button class="btn btn-info">View</button></a></td>
                        <td><a href="{{url('delete_author')}}/{{$author['id']}}"><button class="btn btn-danger">Delete</button></a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <?php $total_page = $author_data['total_pages'] ?>
            <div class="d-flex gap-2">
                @if($total_page > 1)
                @for ($i = 1; $i <= $total_page; $i++) <form method="post" action="{{url('home')}}">
                    {{@csrf_field()}}
                    <input type="hidden" name="page" value="{{$i}}">
                    <button class="btn btn-primary">{{$i}}</button>
                    </form>
                    @endfor
                    @endif
            </div>
        </div>
    </div>
</div>

@stop