<!DOCTYPE html>
<html lang="en">

<head>
    @include('includes.head')
</head>

<body>
    <div class="container-fluid">
        <div class="row justify-content-center align-items-center">
            @include('includes.header')

            @yield('content')
        </div>
    </div>
</body>

</html>